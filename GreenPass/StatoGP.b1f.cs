﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace GreenPass
{
    [FormAttribute("GreenPass.Form1", "StatoGP.b1f")]
    class StatoGP : UserFormBase
    {
        public StatoGP()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_5").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_1").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }

        private SAPbouiCOM.Grid Grid0;

        private void PopolaComboDipendente()
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            if(this.ComboBox0.ValidValues.Count == 0)
            {
                this.ComboBox0.ValidValues.Add("", "");
                oRecordset.DoQuery("SELECT \"empID\",\"lastName\", \"firstName\" FROM OHEM ORDER BY \"lastName\" ");
                while (!oRecordset.EoF)
                {
                    this.ComboBox0.ValidValues.Add(oRecordset.Fields.Item(0).Value.ToString(), oRecordset.Fields.Item(1).Value.ToString()+ " " + oRecordset.Fields.Item(2).Value.ToString());
                    oRecordset.MoveNext();
                }
            }
            this.ComboBox0.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            this.ComboBox0.Item.DisplayDesc = true;
        }

        private void PopolaGriglia()
        {
            string Query = "";
            Query += "SELECT \"lastName\" AS \"Nome\", \"firstName\" AS \"Cognome\", \"U_FO_SCADGP\" AS \"Scadenza Green Pass\",  ";
            Query += "CASE WHEN \"U_FO_SCADGP\" < CURRENT_DATE THEN 'Scaduto'  WHEN \"U_FO_SCADGP\" IS NULL THEN 'Non presente' ELSE 'Valido' END AS \"Stato\" ";
            Query += " FROM OHEM WHERE '1' = '1' ";

            if(ComboBox0.Value.ToString() != "")
            {
                Query += " AND \"empID\" = '" + ComboBox0.Value.ToString() + "'";
            }
            if (EditText1.Value.ToString() != "")
            {
                Query += " AND \"U_FO_SCADGP\" < '" + EditText1.Value.ToString().Substring(0,4) + "-"+ EditText1.Value.ToString().Substring(4, 2) +"-"+EditText1.Value.ToString().Substring(6, 2)+ "'";
            }

            Query += " ORDER BY \"U_FO_SCADGP\" DESC ";

            this.UIAPIRawForm.Freeze(true);
            this.Grid0.DataTable.Clear();
            this.Grid0.DataTable.ExecuteQuery(Query);

            for (int j = 0; j <= this.Grid0.Columns.Count - 1; j++)
            {
                this.Grid0.Columns.Item(j).Editable = false;
                this.Grid0.Columns.Item(j).TitleObject.Sortable = true;
            }

            for (int i = 0; i < this.Grid0.Rows.Count; i++)
            {
                if(this.Grid0.DataTable.GetValue("Stato", i).ToString() == "Scaduto")
                {
                    this.Grid0.CommonSetting.SetRowFontStyle(i + 1, SAPbouiCOM.BoFontStyle.fs_Bold);
                }
            }

            this.Grid0.AutoResizeColumns();

            this.UIAPIRawForm.Freeze(false);



        }

        private void OnCustomInitialize()
        {
            PopolaComboDipendente();
        }
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.ComboBox ComboBox0;
        private SAPbouiCOM.Button Button0;

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            PopolaGriglia();

        }

        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.StaticText0.Item.Left = 10;
            this.StaticText0.Item.Top = 10;
            this.ComboBox0.Item.Top = this.StaticText0.Item.Top;
            this.ComboBox0.Item.Left = this.StaticText0.Item.Left + this.StaticText0.Item.Width;
            this.ComboBox0.Item.Width = this.StaticText0.Item.Width * 2;

            this.StaticText1.Item.Top = this.StaticText0.Item.Top;
            this.StaticText1.Item.Left = this.ComboBox0.Item.Left + this.ComboBox0.Item.Width + 30;
            this.EditText1.Item.Top = this.StaticText1.Item.Top;
            this.EditText1.Item.Left = this.StaticText1.Item.Left + this.StaticText1.Item.Width;

            this.Button0.Item.Top = this.EditText1.Item.Top;
            this.Button0.Item.Left = this.UIAPIRawForm.Width - this.Button0.Item.Width - 30;

            this.Grid0.Item.Top = this.StaticText0.Item.Top + this.StaticText0.Item.Height + 10;
            this.Grid0.Item.Left = this.StaticText0.Item.Left;
            this.Grid0.Item.Width = this.UIAPIRawForm.Width - 20;
            this.Grid0.Item.Height = this.UIAPIRawForm.Height - 20 - this.StaticText0.Item.Height - this.Grid0.Item.Top;
        }
    }
}
