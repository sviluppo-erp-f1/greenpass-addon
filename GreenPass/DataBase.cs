﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManager;

namespace GreenPass
{
    class DataBase : Db
    {
        public DataBase()
        {
            Tables = new DbTable[] {
                new DbTable("@ADDONPAR", "Parametri AddOn", SAPbobsCOM.BoUTBTableType.bott_NoObject),

            };

            Columns = new DbColumn[] {
                
                #region OHEM
                new DbColumn("OHEM", "FO_SCADGP", "Scadenza Green Pass", 
                    SAPbobsCOM.BoFieldTypes.db_Date, SAPbobsCOM.BoFldSubTypes.st_None, 10, new DbValidValue[0],-1,null),
                
                #endregion



            };
        }
    }
}
