
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace GreenPass
{

    [FormAttribute("60100", "Dati anagrafici dipendenti - Sessione di elaborazione UI.b1f")]
    class Dati_anagrafici_dipendenti___Sessione_di_elaborazione_UI : SystemFormBase
    {
        public Dati_anagrafici_dipendenti___Sessione_di_elaborazione_UI()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("480002023").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("480002024").Specific));
            this.OnCustomInitialize();

        }

        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.EditText EditText1;

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }



        private void OnCustomInitialize()
        {

        }

        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.StaticText0.Item.Left = this.StaticText1.Item.Left;
            this.StaticText0.Item.Top = this.StaticText1.Item.Top + this.StaticText1.Item.Height + 3;
            this.StaticText0.Item.Width = this.StaticText1.Item.Width;

            this.EditText0.Item.Left = this.EditText1.Item.Left;
            this.EditText0.Item.Top = this.StaticText0.Item.Top;
            this.EditText0.Item.Width = this.EditText1.Item.Width;

        }
    }
}
